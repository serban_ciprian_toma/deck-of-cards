import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { faDiamond } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Header = () => {
  return (
    <div className="header">
      <Row>
        <Col>
          <h1 className="text-center">
            <span className="h2 mr-1">
              <FontAwesomeIcon icon={faDiamond} />
            </span>{' '}
            Game of Cards{' '}
            <span className="h2 ml-1">
              <FontAwesomeIcon icon={faDiamond} />
            </span>
          </h1>
        </Col>
      </Row>
    </div>
  );
};

export default Header;
