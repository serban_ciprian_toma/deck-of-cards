import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Footer from '../Footer';
import Header from '../Header';

const AppLayout = (props) => {
  const { children } = props;

  return (
    <>
      <div className="container">
        <Header />
        <div className="content">
          <Row>
            <Col>{children}</Col>
          </Row>
        </div>
        <Footer />
      </div>
    </>
  );
};

export default AppLayout;
