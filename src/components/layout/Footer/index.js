import React from 'react';
import './styles.css';
import { Col, Row } from 'react-bootstrap';

const Footer = () => {
  return (
    <footer>
      <Row>
        <Col>
          <span>© 2022 | Game of Cards (v.1)</span>
        </Col>
      </Row>
    </footer>
  );
};

export default Footer;
