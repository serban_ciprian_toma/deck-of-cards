import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Homepage = () => {
  return (
    <>
      <Row>
        <Col>
          <h3 className="text-center mt-5">This is a simple game of cards.</h3>
          <h5 className="text-center">
            <p>Given one Shuffled Deck of Cards and two Players.</p>
            <p>
              Each turn each player takes 1 top card from the deck.
              <br />
              After comparison of the values the cards are going to the player
              that has a higher card. (ex. 10 is higher than 8, King is higher
              than 10)
            </p>
            <p>
              When both of the cards have the same value (ex. two aces) they
              should go back to deck and deck will be reshuffled.
            </p>
            <p>
              Once the deck is empty, the game ends. The player who has more
              cards - wins.
            </p>
          </h5>
        </Col>
      </Row>
      <Row>
        <Col className="text-center">
          <Link to="/game" className="btn btn-warning">
            Start Game <FontAwesomeIcon icon={faArrowRight} />
          </Link>
        </Col>
      </Row>
    </>
  );
};

export default Homepage;
