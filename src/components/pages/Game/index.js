import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Game = () => {
  const codes = {
    2: 2,
    3: 3,
    4: 4,
    5: 5,
    6: 6,
    7: 7,
    8: 8,
    9: 9,
    0: 10,
    J: 12,
    Q: 13,
    K: 14,
    A: 15,
  };

  const [deckId, setDeckId] = useState(0);
  const [playerGetCard, setPlayerGetCard] = useState(0);
  const [player1Card, setPlayer1Card] = useState(false);
  const [player2Card, setPlayer2Card] = useState(false);
  const [collecter, setCollecter] = useState(0);
  const [reshuffle, setReshuffle] = useState(0);
  const [reshuffled, setReshuffled] = useState(0);
  const [playedCards, setPlayedCards] = useState(false);
  const [p1Cards, setP1Cards] = useState([]);
  const [p2Cards, setP2Cards] = useState([]);
  const [nrOfPlayedCards, setNrOfPlayedCards] = useState(0);

  useEffect(() => {
    if (!deckId) {
      axios({
        url: 'https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1',
        method: 'GET',
        headers: { Accept: 'application/json, text/plain, */*' },
      })
        .then((res) => {
          setDeckId(res.data.deck_id);
        })
        .catch(() => {
          // Catch errors if any
          // console.log(error);
        });
    }
  }, [deckId]);

  useEffect(() => {
    if (playerGetCard) {
      axios({
        url: 'https://deckofcardsapi.com/api/deck/' + deckId + '/draw/?count=2',
        method: 'GET',
      })
        .then((res) => {
          setPlayedCards(res.data.cards);
          setPlayer1Card(res.data.cards[0]);
          setPlayer2Card(res.data.cards[1]);
          calculateCollector(res.data.cards);
          setPlayerGetCard(0);
          setReshuffled(0);
        })
        .catch(() => {
          // console.log(error);
        });
    }
  }, [playerGetCard, playedCards]);

  const calculateCollector = (cards) => {
    let p1Card = codes[cards[0].code[0]];
    let p2Card = codes[cards[1].code[0]];

    if (p1Card > p2Card) {
      setCollecter('P1');
    } else if (p1Card < p2Card) {
      setCollecter('P2');
    } else {
      setCollecter(0);
      setReshuffle(1);
    }

    return true;
  };

  const collectCards = (player) => {
    if (player == 'p1') {
      Object.entries(playedCards).forEach(([, item]) => {
        const a = p1Cards;
        a.push(item);
        setP1Cards(a);
        setCollecter(0);
      });
    }
    if (player == 'p2') {
      Object.entries(playedCards).forEach(([, item]) => {
        const a = p2Cards;
        a.push(item);
        setP2Cards(a);
        setCollecter(0);
      });
    }
    const nrOfTotalCards = parseInt(p1Cards.length) + parseInt(p2Cards.length);
    setNrOfPlayedCards(nrOfTotalCards);
  };

  const reshufleCards = () => {
    if (reshuffle) {
      const axiosUrl =
        'https://deckofcardsapi.com/api/deck/' +
        deckId +
        '/return/?cards=' +
        playedCards[0].code +
        ',' +
        playedCards[1].code;

      axios({
        url: axiosUrl,
        method: 'GET',
      })
        .then(() => {
          setReshuffle(0);
          setReshuffled(1);
        })
        .catch(() => {
          // console.log(error);
        });
    }

    axios({
      url:
        'https://deckofcardsapi.com/api/deck/' +
        deckId +
        '/shuffle/?remaining=true',
      method: 'GET',
    })
      .then(() => {
        setReshuffle(0);
        setReshuffled(1);
      })
      .catch(() => {
        // console.log(error);
      });
  };
  return (
    <>
      <Row className="mt-5">
        <Col xs={5}>
          <div className="text-center">Player1</div>
          <Row>
            <Col xs={9}>
              {p1Cards.length > 0 &&
                p1Cards.map((item) => {
                  return item.code + ', ';
                })}
            </Col>
            <Col xs={3} className="text-center">
              {!reshuffled && (
                <img
                  src={player1Card.image}
                  alt={player1Card.code}
                  className="w-100"
                />
              )}

              {collecter == 'P1' && (
                <span
                  className="btn btn-warning"
                  onClick={() => {
                    collectCards('p1');
                  }}
                >
                  Collect Cards
                </span>
              )}
              {collecter == 'P2' && 'Sorry - You lost this round'}
              {reshuffle == 1 && 'Nobody wins'}
            </Col>
          </Row>
        </Col>
        <Col xs={2} className="text-center">
          <div>DECK</div>
          <div>
            <img
              src={require('../../../static/img/deck-back.png')}
              className="w-100"
            />
            {reshuffle ? (
              <span
                className="btn btn-warning"
                onClick={() => {
                  reshufleCards();
                }}
              >
                Reshuffle
              </span>
            ) : nrOfPlayedCards < 52 ? (
              !collecter && (
                <span
                  className="btn btn-success mt-5"
                  onClick={() => {
                    setPlayerGetCard(1);
                  }}
                >
                  Play Card
                </span>
              )
            ) : (
              <div>
                <span>
                  {p1Cards.length > p2Cards.length ? 'Player 1' : 'Player 2'} is
                  the WINNER
                </span>
                <br />
                <span>
                  <Link to="/" className="btn btn-warning">
                    Start A New Game
                  </Link>
                </span>
              </div>
            )}
          </div>
        </Col>
        <Col xs={5}>
          <div className="text-center">Player2</div>
          <Row>
            <Col xs={3} className="text-center">
              {!reshuffled && (
                <img
                  src={player2Card.image}
                  alt={player2Card.code}
                  className="w-100"
                />
              )}
              {collecter == 'P2' && (
                <span
                  className="btn btn-warning"
                  onClick={() => {
                    collectCards('p2');
                  }}
                >
                  Collect Cards
                </span>
              )}
              {collecter == 'P1' && 'Sorry - You lost this round'}
              {reshuffle == 1 && 'Nobody wins'}
            </Col>
            <Col xs={9} className="text-left">
              {p2Cards.length > 0 &&
                p2Cards.map((item) => {
                  return item.code + ', ';
                })}
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default Game;
