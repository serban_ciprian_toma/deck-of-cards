import React from 'react';

const Player = (props) => {
  const cards = props.cards;

  return (
    <>
      {cards.length > 0 &&
        cards.map((item) => {
          return item.code + ', ';
        })}
    </>
  );
};

export default Player;
