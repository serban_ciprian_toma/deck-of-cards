import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';

import './App.css';
import AppLayout from './components/layout/AppLayout';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Homepage from './components/pages/Homepage';
import Game from './components/pages/Game';

function App() {
  return (
    <div className="App">
      <AppLayout>
        <Router>
          <Routes>
            <Route path="/" element={<Homepage />} />
            <Route path="/game" element={<Game />} />
          </Routes>
        </Router>
      </AppLayout>
    </div>
  );
}

export default App;
